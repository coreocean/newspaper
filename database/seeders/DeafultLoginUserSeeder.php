<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DeafultLoginUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Admin Seeder ##
        $user = User::updateOrCreate([
            'email' => 'newsadmin@gmail.com'
        ],[
            'name' => 'Admin',
            'email' => 'newsadmin@gmail.com',
            'mobile' => '9876543210',
            'password' => Hash::make('password'),
        ]);
    }
}
