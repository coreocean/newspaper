import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vuePlugin from '@vitejs/plugin-vue';
import path from 'path';


export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/css/app.css',
                'public/admin/css/style.css',
                'public/admin/css/color-1.css',
                'public/admin/css/responsive.css',
                'resources/js/app.js',
                'public/admin/js/script.js',
                'public/admin/js/theme-customizer/customizer.js',
                // 'public/admin/js/custom-card/custom-card.js',
                // 'public/admin/js/clipboard/clipboard.min.js',
                'public/admin/js/prism/prism.min.js',
                'public/admin/js/sidebar-menu.js',
                'public/admin/js/config.js'
            ],
            refresh: true,
        }),
        vuePlugin({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
    resolve: {
        alias: {
            '@': path.resolve(__dirname, './resources/js'),
            vue: "vue/dist/vue.esm-bundler.js",
        },
    },
});
