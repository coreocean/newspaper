<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
{{-- @php
$css_file = "css/app.css";
@endphp --}}

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Enzo admin is super flexible, powerful, clean &amp; modern responsive bootstrap 5 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Enzo admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="../assets/images/favicon/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="../assets/images/favicon/favicon.png" type="image/x-icon">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ url('favicon.ico') }}">
    <title>
        @yield('title', config('app.name', 'Newspaper advertisement'))
    </title>

    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/font-awesome.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/icofont.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/vendors/scrollbar.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('admin/css/color-1.css') }}" media="screen">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/css/responsive.css') }}"> --}}

    @vite([
        'resources/css/app.css',
        // 'public/admin/css/style.css',
        // 'public/admin/css/color-1.css',
        // 'public/admin/css/responsive.css',
        'resources/js/app.js',
        'public/admin/js/script.js',
        'public/admin/js/theme-customizer/customizer.js',
        // 'public/admin/js/custom-card/custom-card.js',
        // 'public/admin/js/clipboard/clipboard.min.js',
        'public/admin/js/prism/prism.min.js',
        'public/admin/js/sidebar-menu.js',
        'public/admin/js/config.js'
    ])

</head>

<body>

    <div id="app">
        <router-view></router-view>
    </div>

    <!-- built files will be auto injected -->
    <div id="dynamic-scripts"></div>
    @stack('scripts')

    <script>
        window.config = {
            'path': '{{ url('/') }}',
            'app_name': "{{ config('app.name') }}",
        };
    </script>

</body>
</html>
