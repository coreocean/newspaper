import router from '../router'
import { defineStore } from 'pinia'
import { handleError } from '@/helpers/error-handling'

export const useAuthStore = defineStore({
    id: 'auth',
    state: () => ({
        register: {
            name: "",
            email: "",
            password: "",
            confirm_password: "",
        },
        updatePassword: {
            email: "",
            password: "",
            password_confirmation: "",
        },
        formErrors: {}
    }),

    actions: {

        // async login(formData)
        // {
        //     try
        //     {
        //         await axios.get('/sanctum/csrf-cookie');
        //         const response = await axios.post('/auth/login', formData);
        //         console.log(response);
        //         return response;
        //     }
        //     catch(error)
        //     {
        //         handleError(error, this.formErrors);
        //         throw error;
        //     }
        // },

        login(formData) {
            return new Promise((resolve, reject) => {
                axiosAdmin.get('/sanctum/csrf-cookie').then((response) => {
                    if (response) {
                        axiosAdmin.post('/auth/login', formData)
                            .then((response) => {
                                // localStorage.setItem('usertoken', res.data.data.token)
                                resolve(response)
                            })
                            .catch((err) => {
                                handleError(err, this.formErrors)
                                reject(err)
                            })
                    }
                }).catch((err) => {
                    handleError(err)
                    reject(err)
                })
            })
        },

        // updatePassword(data) {
        //     return new Promise((resolve, reject) => {
        //         axios.get('/sanctum/csrf-cookie').then((response) => {
        //             if (response) {
        //                 axios
        //                     .post('/password/reset', data)
        //                     .then((response) => {
        //                         resolve(response)
        //                     })
        //                     .catch((err) => {
        //                         handleError(err)
        //                         reject(err)
        //                     })
        //             }
        //         }).catch((err) => {
        //             handleError(err)
        //             reject(err)
        //         })
        //     })
        // },

        logout() {
            return new Promise((resolve, reject) => {
                axiosAdmin
                    .post('/auth/logout')
                    .then((response) => {
                        router.replace('/login')
                        resolve(response)
                    })
                    .catch((err) => {
                        handleError(err)
                        reject(err)
                    })
            })
        },

        // registerUser(data) {
        //     return new Promise((resolve, reject) => {
        //         axios
        //             .post('/auth/register', data)
        //             .then((response) => {
        //                 resolve(response)
        //             })
        //             .catch((err) => {
        //                 handleError(err)
        //                 reject(err)
        //             })
        //     })
        // },

    },
})
