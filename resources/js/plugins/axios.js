import { useAuthStore } from '@/store/auth'
import Swal from "sweetalert2";
import router from "../router"
import axios from 'axios'
import toastr from 'toastr'

window.axiosAdmin = axios

axiosAdmin.defaults.withCredentials = true
axiosAdmin.defaults.baseURL = 'http://localhost:8000/api';
axiosAdmin.defaults.headers.common = {
        'X-Requested-With': 'XMLHttpRequest',
        'Accept': 'application/json',
        'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
    }

// Axios bearer token by default
if (localStorage.getItem('auth_token')) {
    axiosAdmin.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('auth_token');
}


/**
 * Interceptors
 */

axiosAdmin.interceptors.request.use((config) => {
    return config
})

axiosAdmin.interceptors.response.use(
        (response) => {
            // Call was successful, don't do anything special.
            return response
        },
        (error) => {
            const authStore = useAuthStore()

            switch (error.response.status) {
                case 403:
                    toastr["error"](error.response.data.message)
                    break
                case 401: // Not logged in
                    toastr["error"]("Unauthorized : "+ error.response.data.message)
                    // authStore.logout()
                    break
                case 404:
                    toastr["error"]("404 request not found")
                    break
                // case 422:
                //     handleError(error)
                //     break
                // authStore.logout('error', 'Unauthorized')
                case 419: // Session expired
                    toastr["error"]("Unauthorized!")
                    authStore.logout()
                    break
                case 503: // Down for maintenance
                    // Bounce the user to the login screen with a redirect back
                    toastr["error"]("404 request not found")
                    break
                case 500:
                    toastr["error"](error.response.data.message)
                    break
                default:
                    // Allow individual requests to handle other errors
                    return Promise.reject(error)
            }
        })
    // https://laracasts.com/discuss/channels/general-discussion/detecting-expired-session-with-sanctumspa
