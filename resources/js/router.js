import { createRouter, createWebHistory } from "vue-router";
import {loadLayoutMiddleware} from "@/loadLayoutMiddleware";
// import { useStore } from 'vuex'
// import { useUserStore } from './store/user'

const guestRoutes = [
    {
        path: "/",
        name: "/",
        // meta: { active: 'projects' },
        component: () => import("./views/auth/Login.vue"),
    },
    {
        path: "/login",
        name: "login",
        // meta: { active: 'projects' },
        component: () => import("./views/auth/Login.vue"),
    },
    {
        path: "/",
        name: "register",
        // meta: { active: 'projects' },
        component: () => import("./views/auth/Register.vue"),
    },
    // {
    //     path: '/',
    //     component: () =>
    //         import('./views/auth/layouts/LayoutLogin.vue'),
    //     meta: { requiresAuth: false, redirectIfAuthenticated: true },
    //     children: [{
    //             path: '',
    //             component: () =>
    //                 import('./views/auth/Login.vue'),
    //         },
    //         {
    //             path: 'login',
    //             name: 'login',
    //             component: () =>
    //                 import('./views/auth/Login.vue'),
    //         },
    //         {
    //             path: 'register',
    //             name: 'register',
    //             component: () =>
    //                 import('./views/auth/Register.vue'),
    //         }
    //     ],
    // },
    {
        path: "/:catchAll(.*)",
        name: "404",
        component: () => import("./views/utility/404.vue"),
    },
];

const authRoutes = [
    {
        path: "/",
        name: "admin.dashboard",
        meta: { active: "dashboard" },
        component: () => import("./views/dashboard/Dashboard.vue"),
    },
    {
        path: "/dashboard",
        name: "admin.dashboard",
        meta: { active: "dashboard" },
        component: () => import("./views/dashboard/Dashboard.vue"),
    },
    // {
    //     path: "/dashboard",
    //     component: () => import("./views/layouts/Layout.vue"),
    //     meta: { requiresAuth: true, isAdmin: true },
    //     children: [
    //         // {
    //         //     path: "/dashboard",
    //         //     name: "admin.dashboard",
    //         //     meta: { active: "dashboard" },
    //         //     component: () => import("./views/dashboard/Dashboard.vue"),
    //         // },
    //         {
    //             path: "",
    //             name: "admin.dashboard",
    //             meta: { active: "dashboard" },
    //             component: () => import("./views/layouts/Layout.vue"),
    //         }
    //     ],
    // },
];

const routes = [...guestRoutes, ...authRoutes];

const router = createRouter({
    history: createWebHistory(),
    linkActiveClass: "active",
    routes,
});

// router.beforeEach(loadLayoutMiddleware)

export default router;
