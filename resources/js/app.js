import '@/plugins/axios'
import { createApp } from 'vue'
import router from "./router"
import { createPinia } from 'pinia'
// import $ from 'jquery';
// window.$ = window.jQuery = $;
// import toastr from 'toastr'
import 'sweetalert2/dist/sweetalert2.min.css';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../../public/admin/css/style.css';
import '../../public/admin/css/color-1.css';
import '../../public/admin/css/responsive.css';
import '../../resources/css/app.css';
import '@fortawesome/fontawesome-free/css/all.css';


const app = createApp()

app.use(router)
app.use(createPinia())
app.mount('#app')
