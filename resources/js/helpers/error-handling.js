import { useAuthStore } from "@/store/auth";
import toastr from "toastr";

export const handleError = (err, formErrors = null) => {
    const authStore = useAuthStore();

    if (!err.response)
    {
        toastr["info"](
            "Please check your internet connection or wait until servers are back online."
        );
    }
    else if(err.response.status === 422 && formErrors)
    {
        for (const field in err.response.data.errors) {
            formErrors[field] = err.response.data.errors[field][0]; // Extract first error message
        }
    }
    else
    {
        if ( err.response.data && (err.response.statusText === "Unauthorized" || err.response.data === "Unauthorized.") )
        {
            // Unauthorized and log out
            const msg = err.response.data.message ? err.response.data.message : "Unauthorized";
            // showToaster(msg);
            authStore.logout();
        }
        else if (err.response.data.errors)
        {
            // Show a notification per error
            const errors = JSON.parse(JSON.stringify(err.response.data.errors));
            for (const i in errors) showToaster(errors[i][0]);
        }
        else if (err.response.data.error)
        {
            if (typeof err.response.data.error == "boolean")
                showToaster(err.response.data?.message);
            else showToaster(err.response.data.error);
        }
        else
        {
            showToaster(err.response.data.message);
        }
    }
};

export const showError = (error) => {
    // show any custom error message using this method
    switch (error) {
        default:
            showToaster(error);
            break;
    }
};

export const showToaster = (msg) => {
    toastr["error"](msg);
};
