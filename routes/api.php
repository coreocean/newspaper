<?php

use App\Http\Controllers\Api\Auth\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix("auth")->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::post('/logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
    // Route::get('{provider}', [SocialAccountController::class, 'redirectToProvider']);
});


Route::middleware(['auth:sanctum','verified'])->group(function () {

    Route::prefix('/admin')->group(function () {
        // Route::get('/dashboard', [DashboardController::class, 'index']);
        // Route::get('/users', [DashboardController::class, 'users']);
        // Route::get('/projects', [DashboardController::class, 'projects']);
    });

});
