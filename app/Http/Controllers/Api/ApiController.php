<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;


class ApiController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function respondWith($data, $success, $message = "Action successful.", $code = 200, $redirect = null, ...$params)
    {
        $response = [
            'success' => $success ?? true,
            'data' => $data,
            'message' => $message,
            'errors' => [],
        ];

        // foreach($params as $param)
        // {
        //     $response['meta'][] = $param;
        // }

        if (!empty($redirect))
            $response['redirect'] = $redirect;

        if ($params)
            $response['meta'] = $params;

        return response()->json($response, $code);
    }
}
