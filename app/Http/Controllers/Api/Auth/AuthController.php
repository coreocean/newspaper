<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends ApiController
{
    public function register(RegisterRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'mobile' => $request->mobile,
            'password' => Hash::make($request->password)
        ]);

        $token = explode('|', $user->createToken('auth_token')->plainTextToken, 2);
        $user->refresh();
        $user['token'] = $token[1];

        return $this->respondWith(true, UserResource::make($user));
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password))
        {
            return $this->respondWith([], false, "Credentials does not match", 500, null );
        }

        $token = explode('|', $user->createToken('auth_token')->plainTextToken, 2);
        $user->refresh();
        $user['token'] = $token[1];

        return $this->respondWith(UserResource::make($user), true );
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
        return $this->respondWith([], true);
    }
}
